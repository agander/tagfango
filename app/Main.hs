--[># LANGUAGE OverloadedStrings #<]

module Main where

import qualified TagfangoLib as TL
--import TagfangoLib
import Network.Curl
import Text.HTML.TagSoup
--import Debug.Trace
import Text.Printf
import Text.Regex.PCRE.Light
--import Text.Regex.PCRE.Light.Extra
--import qualified Data.Text as T
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as B8
import qualified Data.ByteString.Lazy as BSL

main :: IO ()
main = do
  {-
  putStrLn "D:20"
  src <- curlGetString "https://www.stackage.org/" []
  let tags = parseTags $ snd src
  putStrLn "D:24"
  putStrLn "D:25"
  let lts_list = fromFooter $ parseTags $ snd src
  putStrLn $ "D:27:lts:  *" ++ lts_list ++ "*"
  putStrLn "D:28"
  let links = map f $ sections (~== "<a>") $
              takeWhile (~/= "</ul>") $
              drop 5 $
              dropWhile (~/= "<ul>") tags
  putStrLn "D:33"
  putStr "links: *"
  mapM my_putstr links
  putStrLn "*"
  -- | "a href="https://www.stackage.org/"
  let tags2 = parseTags TL.frag1
  putStrLn "D:39"
  src3 <- curlGetString "https://www.timeanddate.com/worldclock/italy/palermo" []
  let tags3 = parseTags $ snd src3
  --let time = fromTagText (dropWhile (~/= "<span id=ct>") tags3 !! 1)
  let time = fromTagText (dropWhile (~/= TagOpen "span" [("id","ct")]) tags3 !! 1)
  putStrLn ("PMO: " ++ time)
  print TL.parseTable
  putStrLn "D:46"
  --putStrLn $ innerText $ sections (~== "<tr>") TL.frag2
  putStrLn "D:48"
  mapM_ print $ map f $ sections (~== "<a>") $ takeWhile (~/= "a") $ dropWhile (~/= "<li>") tags
  let all_list = map f $ sections (~== "<a>") $ takeWhile (~/= "a") $ dropWhile (~/= "<li>") tags
  printf "all_list length: %d\n" $ length all_list
  -- | LTS \d{1,}.\d{1,} for ghc-\d{1,}.\d{1,}.\d{1,}
  -- "LTS 0.7 for ghc-7.8.3"
  --let pat = build_pat
  let r = compile build_pat

  let bs_list = map B8.pack all_list
  -}
  --mapM printf "bs_list. %s\n" bs_list
  --let net_list = map (match r []) bs_list
  let lst = TL.listOfCountriesOfTheWorld
  --mapM putStrLn lst
  return ()

