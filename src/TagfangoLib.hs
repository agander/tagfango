{-# LANGUAGE QuasiQuotes #-}
module TagfangoLib (
  module TagSoup.Test
  , module TagSoup.Benchmark
  , module TagSoup.Sample
  , frag1
  , frag2
  , frag3
  , listOfCountriesOfTheWorld
)
where

import TagSoup.Test
import TagSoup.Benchmark
import TagSoup.Sample
import Text.Heredoc
import qualified Data.ByteString.Char8 as B8
import Network.Curl

frag1 = [str|<h3>Latest LTS per GHC version</h3>
|<ul><li><a href="https://www.stackage.org/lts-15.9">LTS 15.9 for ghc-8.8.3</a>, published 4 days ago</li>
|<li><a href="https://www.stackage.org/lts-15.3">LTS 15.3 for ghc-8.8.2</a>, published a month ago</li>
|<li><a href="https://www.stackage.org/lts-14.27">LTS 14.27 for ghc-8.6.5</a>, published 2 months ago</li>
|<li><a href="https://www.stackage.org/lts-13.19">LTS 13.19 for ghc-8.6.4</a>, published a year ago</li>
|<li><a href="https://www.stackage.org/lts-13.11">LTS 13.11 for ghc-8.6.3</a>, published a year ago</li>
|<li><a href="https://www.stackage.org/lts-12.26">LTS 12.26 for ghc-8.4.4</a>, published a year ago</li>
|<li><a href="https://www.stackage.org/lts-12.14">LTS 12.14 for ghc-8.4.3</a>, published a year ago</li>
|<li><a href="https://www.stackage.org/lts-11.22">LTS 11.22 for ghc-8.2.2</a>, published a year ago</li>
|<li><a href="https://www.stackage.org/lts-9.21">LTS 9.21 for ghc-8.0.2</a>, published 2 years ago</li>
|<li><a href="https://www.stackage.org/lts-7.24">LTS 7.24 for ghc-8.0.1</a>, published 3 years ago</li>
|<li><a href="https://www.stackage.org/lts-6.35">LTS 6.35 for ghc-7.10.3</a>, published 3 years ago</li>
|<li><a href="https://www.stackage.org/lts-3.22">LTS 3.22 for ghc-7.10.2</a>, published 4 years ago</li>
|<li><a href="https://www.stackage.org/lts-2.22">LTS 2.22 for ghc-7.8.4</a>, published 5 years ago</li>
|<li><a href="https://www.stackage.org/lts-0.7">LTS 0.7 for ghc-7.8.3</a>, published 5 years ago</li>
|</ul>
<h3>Package Maintainers</h3>|]

frag2 = [str|<table>
|<tr>
|<th>Company</th>
|<th>Contact</th>
|<th>Country</th>
|</tr>
|<tr>
|<td>Alfreds Futterkiste</td>
|<td>Maria Anders</td>
|<td>Germany</td>
|</tr>
|<tr>
|<td>Centro comercial Moctezuma</td>
|<td>Francisco Chang</td>
|<td>Mexico</td>
|</tr>
|<tr>
|<td>Ernst Handel</td>
|<td>Roland Mendel</td>
|<td>Austria</td>
|</tr>
|<tr>
|<td>Island Trading</td>
|<td>Helen Bennett</td>
|<td>UK</td>
|</tr>
|<td>Laughing Bacchus Winecellars</td>
|<td>Yoshi Tannamuri</td>
|<td>Canada</td>
|</tr>
|<tr>
|<td>Magazzini Alimentari Riuniti</td>
|<td>Giovanni Rovelli</td>
|<td>Italy</td>
|</tr>
|</table>|]


frag3 = [str|<h3>Related initiatives</h3>
|<p>Stack is the recommended way to use Stackage.</p>
|<p><a href="https://haskell.fpcomplete.com/get-started">Get started with Stack</a>
|on
|<a href="https://haskell.fpcomplete.com/">haskell.fpcomplete.com</a>.</p>
|<h3>Latest LTS per GHC version</h3>
|<ul><li><a href="https://www.stackage.org/lts-15.9">LTS 15.9 for ghc-8.8.3</a>, published 4 days ago</li>
|<li><a href="https://www.stackage.org/lts-15.3">LTS 15.3 for ghc-8.8.2</a>, published a month ago</li>
|<li><a href="https://www.stackage.org/lts-14.27">LTS 14.27 for ghc-8.6.5</a>, published 2 months ago</li>
|<li><a href="https://www.stackage.org/lts-13.19">LTS 13.19 for ghc-8.6.4</a>, published a year ago</li>
|<li><a href="https://www.stackage.org/lts-13.11">LTS 13.11 for ghc-8.6.3</a>, published a year ago</li>
|<li><a href="https://www.stackage.org/lts-12.26">LTS 12.26 for ghc-8.4.4</a>, published a year ago</li>
|<li><a href="https://www.stackage.org/lts-12.14">LTS 12.14 for ghc-8.4.3</a>, published a year ago</li>
|<li><a href="https://www.stackage.org/lts-11.22">LTS 11.22 for ghc-8.2.2</a>, published a year ago</li>
|<li><a href="https://www.stackage.org/lts-9.21">LTS 9.21 for ghc-8.0.2</a>, published 2 years ago</li>
|<li><a href="https://www.stackage.org/lts-7.24">LTS 7.24 for ghc-8.0.1</a>, published 3 years ago</li>
|<li><a href="https://www.stackage.org/lts-6.35">LTS 6.35 for ghc-7.10.3</a>, published 3 years ago</li>
|<li><a href="https://www.stackage.org/lts-3.22">LTS 3.22 for ghc-7.10.2</a>, published 4 years ago</li>
|<li><a href="https://www.stackage.org/lts-2.22">LTS 2.22 for ghc-7.8.4</a>, published 5 years ago</li>
|<li><a href="https://www.stackage.org/lts-0.7">LTS 0.7 for ghc-7.8.3</a>, published 5 years ago</li>
|</ul>
|<h3>Package Maintainers</h3>|]

-- | countries of the world..
--listOfCountriesOfTheWorld :: IO ([])
listOfCountriesOfTheWorld  = do
  putStrLn "D:lib:102"
  src4 <- curlGetString "https://www.listofcountriesoftheworld.com" []
  let tags4 = parseTags $ snd src4
  --mapM_ print $ map f $ sections (~== "<a>") $ takeWhile (~/= "id=\"ctry\"") $ dropWhile (~/= "<a href>") tags4
  return ( map f $ sections (~== "<a>") $ takeWhile (~/= "id=\"ctry\"") $ dropWhile (~/= "<a href>") tags4 )

f :: [Tag String] -> String
f = dequote . unwords . words . fromTagText . head . filter isTagText

dequote ('\"':xs) | last xs == '\"' = init xs
dequote x = x

my_putstr :: String -> IO ()
my_putstr str = putStr $ str ++ ", "

--fromFooter2 = innerText .  takeWhile (~/= "<h3>Package Maintainers") .  dropWhile (~/= "<h3>Latest LTS per GHC version")
--fromFooter2 = innerText . takeWhile (~/= "<h3>Package Maintainers") . dropWhile (~/= TagOpen "h3" [] TagText "Latest LTS per GHC version")
-- | attempt 1:
-- | fromFooter = innerText . take 2 . dropWhile (~/= "<ul><li>")
-- | res:
-- | tagfango: When using a TagRep it must be exactly one tag, you gave: <ul><li>
-- | attempt 2:
--fromFooter = innerText . take 2 . dropWhile (~/= "Latest LTS per GHC version")
-- | res:
-- | "Latest releases" - found in the 1st <h3> tag
-- | attempt 3:
-- | fromFooter = innerText . take 2 . dropWhile (~/= "<a>")
-- | res:
-- | empty result
-- | fromFooter = fromTagText . head . filter isTagText . dropWhile (~/= "<a href>")
-- | lts:  *
-- | *
-- | links: **
-- | fromFooter = innerText . takeWhile (~/= "<h3>Package Maintainers") . dropWhile (~/= [TagOpen "h3" [] TagText "Latest LTS per GHC version"])
-- | res:
-- | empty result

build_pat :: B8.ByteString
-- build_pat pat = "([^/])*([\\w\\s-\\.]*" ++ pat ++ "[\\w\\s-\\.]*)$"
build_pat = B8.pack "LTS \\d{1,}.\\d{1,} for ghc-\\d{1,}.\\d{1,}.\\d{1,}"

